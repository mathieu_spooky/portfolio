import {
  useTranslations,
  useFormatter,
  type DateTimeFormatOptions,
} from "next-intl";

import { HeaderNavigationBar } from "@/components/product/header-navigation-bar";
import { ProfileBanner } from "@/components/product/profile-banner";
import { AboutMe } from "@/components/product/about-me";
import { Technologies } from "@/components/product/technologies";
import { Typo } from "@/components/ui/atoms/typo";
import { EnacoMissionCard } from "@/components/product/enaco-mission-card";
import { LastMission } from "@/components/product/last-mission";
import { ClicDataMissionCard } from "@/components/product/clic-data-mission-card";
import { Testimonials } from "@/components/product/testimonials";

import { TranslationsStoreProvider } from "@/providers/translations-store-provider";

const enacoDateRange = {
  startDate: new Date("2020-06-01"),
  endDate: new Date("2023-09-01"),
};

const clicDataDate = new Date("2023-01-01");

const payByStepDateRange = {
  startDate: new Date("2023-09-01"),
  endDate: new Date("2024-01-01"),
};

const dateFormat = {
  year: "numeric",
  month: "long",
} satisfies DateTimeFormatOptions;

export default function Home() {
  const t = useTranslations();
  const format = useFormatter();

  const translations = {
    aboutMe: t("navbar.about-me"),
    lastMission: t("navbar.last-mission"),
    portfolio: t("navbar.portfolio"),
    reviews: t("navbar.reviews"),
    designerUiUx: t("profile-banner.designer-ui-ux"),
    designerUiUxDescription: t("profile-banner.designer-ui-ux-description"),
    developer: t("profile-banner.developer"),
    developerDescription: t("profile-banner.developer-description"),
    aboutMeTitle: t("about-me.title"),
    aboutMeDescription: t.rich("about-me.description"),
    technologiesTitle: t("technologies.title"),
    lastMissionTitle: t("last-mission.title"),
    lastMissionDescription: t.rich("last-mission.description"),
    clicDataDescription: t.rich("clic-data.description"),
    enacoDescription: t.rich("enaco.description"),
    reviewsTitle: t("reviews.title"),
    reviewsPayByStepJob: t("reviews.paybystep.job"),
    reviewsPayByStepText: t("reviews.paybystep.text"),
    reviewsClicDataJob: t("reviews.clicdata.job"),
    reviewsClicDataText: t("reviews.clicdata.text"),
  };

  const enacoDateRangeFormatted = format.dateTimeRange(
    enacoDateRange.startDate,
    enacoDateRange.endDate,
    dateFormat
  );

  const clicDataDateFormatted = format.dateTime(clicDataDate, dateFormat);

  const payByStepDateRangeFormatted = format.dateTimeRange(
    payByStepDateRange.startDate,
    payByStepDateRange.endDate,
    dateFormat
  );

  return (
    <TranslationsStoreProvider translations={translations}>
      <main className="pb-4">
        <div className="flex flex-col items-center">
          <HeaderNavigationBar className="z-20 w-full sm:sticky sm:top-0" />
          <div className="w-full px-4">
            <ProfileBanner className="sticky top-16 z-10" />
          </div>
          <div className="z-10 -mt-8 w-full flex-1 px-2 text-sm sm:px-4 md:-mt-16">
            <div className="w-full rounded-xl bg-white drop-shadow-2xl">
              <div className="container mx-auto flex flex-col gap-8 px-4 py-8 md:flex-row md:px-8">
                <AboutMe id="aboutMe" className="flex-1 gap-8" />
                <Technologies className="bg-white md:w-[calc(33.3333%-0.5rem)] md:border-l md:border-gray-300 md:pl-8" />
              </div>
              <div className="w-full bg-gray-100">
                <div className="container w-full px-4 py-8 md:px-8">
                  <LastMission
                    id="lastMission"
                    date={payByStepDateRangeFormatted}
                  />
                </div>
              </div>
              <div className="container space-y-4 px-4 py-8 md:px-8">
                <Typo id="portfolio" variant="h2">
                  Portfolio
                </Typo>
                <ClicDataMissionCard date={clicDataDateFormatted} />
                <EnacoMissionCard date={enacoDateRangeFormatted} />
              </div>

              <div className="container space-y-4 px-4 py-8 md:px-8">
                <Typo id="testimonials" variant="h2">
                  {translations.reviewsTitle}
                </Typo>
                <Testimonials />
              </div>
            </div>
          </div>
        </div>
      </main>
    </TranslationsStoreProvider>
  );
}
