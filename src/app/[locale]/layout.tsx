import type { Metadata, Viewport } from "next";
import { Analytics } from "@vercel/analytics/react";
import { GradientBackground } from "@/components/ui/atoms/gradient-background";

import { Karla, Source_Code_Pro, Lora } from "next/font/google";
import "./globals.css";

const montserratAlternates = Lora({
  weight: ["700"],
  subsets: ["latin"],
  variable: "--title-font",
});

const urbanist = Karla({
  weight: ["400", "700"],
  subsets: ["latin"],
  variable: "--body-font",
});

const sourceCodePro = Source_Code_Pro({
  weight: ["400", "700"],
  subsets: ["latin"],
  variable: "--monospace-font",
});

export const metadata: Metadata = {
  title: "Portfolio - Mathieu Dervaux",
  metadataBase: new URL("https://mathieu-dervaux.com"),
  description:
    "Vous recherchez un développeur web ? un designer ? vous êtes au bon endroit !",
};

export const viewport: Viewport = {
  width: "device-width",
  initialScale: 1,
  themeColor: [
    { media: "(prefers-color-scheme: light)", color: "#67e8f9" },
    { media: "(prefers-color-scheme: dark)", color: "#202020" },
  ],
};

export default function RootLayout({
  children,
  params: { locale },
}: Readonly<{
  children: React.ReactNode;
  params: { locale: string };
}>) {
  const fontVariables = `${montserratAlternates.variable} ${urbanist.variable} ${sourceCodePro.variable}`;
  return (
    <html lang={locale} className=" snap-y scroll-pt-24">
      <body className={fontVariables}>
        {children}
        <GradientBackground position="fixed" />
        <Analytics />
      </body>
    </html>
  );
}
