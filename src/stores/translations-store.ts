import type { JSXElementConstructor, ReactNodeArray } from "react";

import { type ReactElement } from "react";
import { createStore } from "zustand/vanilla";

export type RichMessages =
  | string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  | ReactElement<any, string | JSXElementConstructor<any>>
  | ReactNodeArray;

export type TranslationsState = {
  translations: Record<string, string | RichMessages>;
};

export type TranslationsActions = {
  setTranslations: (
    translations: Record<string, string | RichMessages>
  ) => void;
};

export type TranslationsStore = TranslationsState & TranslationsActions;

const initTranslationsStore = (
  translations: Record<string, RichMessages | string>
): TranslationsState => ({
  translations,
});

const defaultTranslationsState: TranslationsState = {
  translations: {},
};

const createTranslationsStore = (
  initialState: TranslationsState = defaultTranslationsState
) => {
  return createStore<TranslationsStore>()((set) => ({
    ...initialState,
    setTranslations: (translations) => set({ translations }),
  }));
};

export { createTranslationsStore, initTranslationsStore };
