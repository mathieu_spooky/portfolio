"use client";

import { type ReactNode, createContext, useRef, useContext } from "react";
import { type StoreApi, useStore } from "zustand";

import type { RichMessages } from "@/stores/translations-store";

import {
  type TranslationsStore,
  createTranslationsStore,
  initTranslationsStore,
} from "@/stores/translations-store";

const TranslationsStoreContext =
  createContext<StoreApi<TranslationsStore> | null>(null);

export interface TranslationsStoreProviderProps {
  children: ReactNode;
  translations: Record<string, string | RichMessages>;
}

const TranslationsStoreProvider = ({
  children,
  translations,
}: TranslationsStoreProviderProps) => {
  const storeRef = useRef<StoreApi<TranslationsStore>>();
  if (!storeRef.current) {
    storeRef.current = createTranslationsStore(
      initTranslationsStore(translations)
    );
  }
  return (
    <TranslationsStoreContext.Provider value={storeRef.current}>
      {children}
    </TranslationsStoreContext.Provider>
  );
};

const useTranslationsStore = <T,>(
  selector: (store: TranslationsStore) => T
): T => {
  const translationsStoreContext = useContext(TranslationsStoreContext);
  if (!translationsStoreContext) {
    throw new Error(
      "useTranslationsStore must be used within a TranslationsStoreProvider"
    );
  }
  return useStore(translationsStoreContext, selector);
};

export { TranslationsStoreProvider, useTranslationsStore };
