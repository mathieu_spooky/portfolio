"use client";

import { type HTMLAttributes, useRef, useEffect } from "react";
import Image from "next/image";
import { cn } from "@/lib/utils";

import { Typo } from "@/components/ui/atoms/typo";
import { useMediaQuery } from "@/hooks/use-media-query";
import { useTranslationsStore } from "@/providers/translations-store-provider";
import { useIsVisible } from "@/hooks/use-is-visible";

export interface ProfileBannerProps extends HTMLAttributes<HTMLDivElement> {}

const maxRotate = 20;
const minRotate = -20;
const angleAdjust = 30;

const ProfileBanner = ({ className, ...props }: ProfileBannerProps) => {
  const { translations } = useTranslationsStore((state) => state);
  const designer = useRef(null);
  const developer = useRef(null);
  const image = useRef(null);
  const isDesktop = useMediaQuery("(min-width: 768px)");
  const ref = useRef(null);
  const isVisible = useIsVisible(ref);

  const calculateCenterAndOffset = (
    element: HTMLElement,
    clientX: number,
    clientY: number
  ) => {
    const rect = element.getBoundingClientRect();
    const centerX = rect.left + rect.width / 2;
    const centerY = rect.top + rect.height / 2;
    const offsetX = Math.max(
      minRotate,
      Math.min(maxRotate, (clientX - centerX) / angleAdjust)
    );
    const offsetY = Math.max(
      minRotate,
      Math.min(maxRotate, (clientY - centerY) / angleAdjust)
    );
    return { offsetX, offsetY };
  };

  // change perspective on mouse move
  useEffect(() => {
    const handleMouseMove = (e: MouseEvent) => {
      if (!isDesktop) return;
      const { clientX, clientY } = e;
      if (!designer.current || !developer.current || !image.current) return;

      const designerElement = designer.current as HTMLElement;
      const developerElement = developer.current as HTMLElement;
      const imageElement = image.current as HTMLElement;

      const { offsetX: offsetXDes, offsetY: offsetYDes } =
        calculateCenterAndOffset(designerElement, clientX, clientY);
      const { offsetX: offsetXDev, offsetY: offsetYDev } =
        calculateCenterAndOffset(developerElement, clientX, clientY);
      const { offsetX: offsetXImg, offsetY: offsetYImg } =
        calculateCenterAndOffset(imageElement, clientX, clientY);

      designerElement.style.transform = `perspective(1000px) rotateY(${offsetXDes}deg) rotateX(${-offsetYDes}deg)`;
      developerElement.style.transform = `perspective(1000px) rotateY(${offsetXDev}deg) rotateX(${-offsetYDev}deg)`;
      imageElement.style.transform = `perspective(1000px) rotateY(${offsetXImg}deg) rotateX(${-offsetYImg}deg)`;
    };

    window.addEventListener("mousemove", handleMouseMove);
    return () => {
      window.removeEventListener("mousemove", handleMouseMove);
    };
  }, [isDesktop]);

  return (
    <div
      ref={ref}
      className={cn(
        "sticky top-16 z-10 container flex flex-col lg:flex-row items-start justify-between text-sm pt-8",
        isVisible ? "animate-appear" : "opacity-0",
        className
      )}
      {...props}
    >
      <div
        ref={designer}
        className="flex w-full flex-col items-center gap-2 border-b border-gray-950/20 py-4 drop-shadow-lg md:border-none md:py-8 lg:flex-1 lg:items-start"
      >
        <Typo
          variant="h1"
          className="w-full text-center text-gray-950 lg:text-left"
        >
          {translations.designerUiUx}
        </Typo>
        <Typo
          variant="p"
          className="w-full text-center text-gray-700 lg:text-left"
        >
          {translations.designerUiUxDescription}
        </Typo>
      </div>

      <div className="order-last mx-auto h-auto w-56 drop-shadow-lg md:w-auto lg:order-none">
        <Image
          ref={image}
          className="size-auto"
          src="/profilePicture.png"
          width={362}
          height={325}
          alt="profile picture"
        />
      </div>

      <div
        ref={developer}
        className="flex w-full flex-col items-center gap-2 py-4 drop-shadow-lg md:py-8 lg:flex-1 lg:items-start"
      >
        <Typo
          variant="h1"
          className="w-full text-center text-gray-950 lg:text-right"
          monospace
        >
          {translations.developer}
        </Typo>
        <Typo
          variant="p"
          className="w-full text-center text-gray-700 lg:text-right"
          monospace
        >
          {translations.developerDescription}
        </Typo>
      </div>
    </div>
  );
};

export { ProfileBanner };
