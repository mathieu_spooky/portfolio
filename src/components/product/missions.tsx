import { forwardRef, type ComponentProps, type HTMLAttributes } from "react";
import { Clock } from "lucide-react";

import { cn } from "@/lib/utils";
import { Typo } from "@/components/ui/atoms/typo";
import { Badge } from "@/components/ui/atoms/badge";
import { Card } from "../ui/atoms/card";

export interface MissionHeaderProps extends HTMLAttributes<HTMLDivElement> {
  name: string;
  competences: {
    name: string;
    color: "blue" | "destructive" | "teal";
  }[];
  date: string;
}

const MissionHeader = ({
  name,
  competences,
  date,
  className,
  ...props
}: MissionHeaderProps) => {
  return (
    <div className={cn("flex flex-col gap-1", className)} {...props}>
      <div className="flex flex-1 flex-row justify-between gap-2 lg:flex-row">
        <Typo variant="h3" className="text-gray-950">
          {name}
        </Typo>
        <div className="flex flex-row items-center gap-2">
          {competences.map((competence) => (
            <Badge key={competence.name} variant={competence.color}>
              {competence.name}
            </Badge>
          ))}
        </div>
      </div>

      <div className="flex flex-row items-center gap-1 self-start text-gray-500">
        <Clock className="size-3" />
        <Typo variant="span">{date}</Typo>
      </div>
    </div>
  );
};

export interface MissionCardProps extends HTMLAttributes<HTMLDivElement> {}

const MissionCard = forwardRef<HTMLDivElement, ComponentProps<typeof Card>>(
  ({ children, className, ...props }, ref) => {
    return (
      <Card
        ref={ref}
        className={cn(
          "text-start flex flex-col md:flex-row gap-0 items-stretch md:hover:scale-[1.01] md:hover:border-1 md:hover:border-teal-500 transition-transform duration-300 ease-in-out group md:hover:shadow-md focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-teal-500 focus-visible:ring-offset-2",
          className
        )}
        {...props}
        asChild
      >
        <button>{children}</button>
      </Card>
    );
  }
);
MissionCard.displayName = "MissionCard";

export interface MissionCardImageProps extends HTMLAttributes<HTMLDivElement> {}

const MissionCardImage = ({ className, ...props }: MissionCardImageProps) => {
  return (
    <div className={cn("flex md:w-1/3 flex-col gap-4", className)} {...props} />
  );
};

export interface MissionCardContentProps
  extends HTMLAttributes<HTMLDivElement> {}

const MissionCardContent = ({
  className,
  ...props
}: MissionCardContentProps) => {
  return (
    <div
      className={cn(
        "flex flex-1 flex-col justify-between gap-1 md:p-4 md:border-l border-gray-300 py-2",
        className
      )}
      {...props}
    />
  );
};

export { MissionHeader, MissionCard, MissionCardContent, MissionCardImage };
