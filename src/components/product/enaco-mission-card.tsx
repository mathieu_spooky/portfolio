"use client";

import { type HTMLAttributes, useRef } from "react";
import Image from "next/image";

import { cn } from "@/lib/utils";
import {
  MissionCard,
  MissionCardContent,
  MissionCardImage,
  MissionHeader,
} from "@/components/product/missions";
import {
  ResponsiveDialog,
  ResponsiveDialogTrigger,
  ResponsiveDialogContent,
} from "@/components/ui/molecules/responsive-dialog";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselPoints,
  CarouselPrevious,
  CarouselNext,
} from "@/components/ui/molecules/carousel";

import { useTranslationsStore } from "@/providers/translations-store-provider";

import { Typo } from "@/components/ui/atoms/typo";
import { useMediaQuery } from "@/hooks/use-media-query";
import { useIsVisible } from "@/hooks/use-is-visible";

export interface EnacoMissionCardProps extends HTMLAttributes<HTMLDivElement> {
  date: string;
}

const EnacoMissionCard = ({
  className,
  date,
  ...props
}: EnacoMissionCardProps) => {
  const { translations } = useTranslationsStore((state) => state);
  const isDesktop = useMediaQuery("(min-width: 768px)");
  const ref = useRef(null);
  const isVisible = useIsVisible(ref);

  return (
    <div
      ref={ref}
      className={cn(isVisible ? "animate-appear" : "opacity-0")}
      {...props}
    >
      <ResponsiveDialog>
        <ResponsiveDialogTrigger asChild>
          <MissionCard className={cn("group", className)}>
            <MissionCardImage>
              {isDesktop ? (
                <Image
                  src="/lesson.jpg"
                  width={1728}
                  height={1117}
                  alt="clic data illustration"
                />
              ) : (
                <EnacoCarousel />
              )}
            </MissionCardImage>

            <MissionCardContent>
              <div className="flex flex-col gap-4">
                <MissionHeader
                  name="Enaco"
                  competences={[
                    { name: "React", color: "blue" },
                    { name: "Figma", color: "destructive" },
                    { name: "Design system", color: "teal" },
                  ]}
                  date={date}
                />

                <div className="line-clamp-2 text-gray-700">
                  {translations.enacoDescription}
                </div>
              </div>

              <Typo
                variant="p"
                className="font-bold text-teal-600 underline-offset-4 group-hover:underline"
              >
                En savoir plus
              </Typo>
            </MissionCardContent>
          </MissionCard>
        </ResponsiveDialogTrigger>
        <ResponsiveDialogContent>
          <div>
            <EnacoCarousel />
            <div className={`flex flex-col gap-4 ${isDesktop ? "p-8" : "p-4"}`}>
              <MissionHeader
                name="Enaco"
                competences={[
                  { name: "React", color: "blue" },
                  { name: "Figma", color: "destructive" },
                  { name: "Design system", color: "teal" },
                ]}
                date={date}
              />
              <div className="space-y-4 text-gray-700">
                {translations.enacoDescription}
              </div>
            </div>
          </div>
        </ResponsiveDialogContent>
      </ResponsiveDialog>
    </div>
  );
};

const EnacoCarousel = () => {
  const isDesktop = useMediaQuery("(min-width: 768px)");
  return (
    <Carousel className={`${isDesktop ? "" : "pt-4"}`}>
      <CarouselContent>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/lesson.jpg"
            alt="Enaco lesson page"
            width={1728}
            height={1117}
          />
        </CarouselItem>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/homepage-d.jpg"
            alt="Enaco home page"
            width={1728}
            height={1117}
          />
        </CarouselItem>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/ds-buttons.jpg"
            alt="Enaco design system buttons"
            width={1728}
            height={1117}
          />
        </CarouselItem>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/ds-colors.jpg"
            alt="Enaco desing system colors"
            width={1728}
            height={1117}
          />
        </CarouselItem>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/ds-avatars.jpg"
            alt="Enaco design system avatars"
            width={1728}
            height={1117}
          />
        </CarouselItem>
      </CarouselContent>
      <CarouselPoints />
      {isDesktop && <CarouselPrevious />}
      {isDesktop && <CarouselNext />}
    </Carousel>
  );
};

export { EnacoMissionCard };
