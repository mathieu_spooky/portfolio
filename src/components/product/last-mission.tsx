"use client";

import { type HTMLAttributes, useRef, useEffect } from "react";
import Image from "next/image";

import { cn } from "@/lib/utils";
import { Typo } from "@/components/ui/atoms/typo";
import { ImagePreview } from "@/components/product/image-preview";
import { MissionHeader } from "@/components/product/missions";
import { useMediaQuery } from "@/hooks/use-media-query";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselPoints,
} from "../ui/molecules/carousel";
import { useTranslationsStore } from "@/providers/translations-store-provider";
import { useIsVisible } from "@/hooks/use-is-visible";

export interface LastMissionProps extends HTMLAttributes<HTMLDivElement> {
  date: string;
}

const LastMission = ({ className, date, ...props }: LastMissionProps) => {
  const { translations } = useTranslationsStore((state) => state);
  const isDesktop = useMediaQuery("(min-width: 768px)");
  const ref = useRef(null);
  const isVisible = useIsVisible(ref);
  const isFirstRender = useRef(true);

  useEffect(() => {
    setTimeout(() => {
      isFirstRender.current = false;
    }, 300);
  }, []);

  return (
    <div
      className={cn(
        "flex flex-1 flex-col gap-8 text-gray-700",
        isVisible ? "animate-appear" : "opacity-0",
        isFirstRender.current ? "delay-300" : "delay-0",
        className
      )}
      ref={ref}
      {...props}
    >
      <Typo variant="h2" className="text-gray-950">
        {translations.lastMissionTitle}
      </Typo>
      <MissionHeader
        name="PayByStep"
        competences={[
          { name: "React", color: "blue" },
          { name: "Figma", color: "destructive" },
          { name: "Design system", color: "teal" },
        ]}
        date={date}
      />
      {isDesktop ? (
        <div className="flex flex-col gap-2 md:flex-row md:gap-4">
          <div className="flex flex-1 flex-col gap-4">
            <ImagePreview
              src="/homepage.jpg"
              alt="PayByStep homepage preview"
              priority
            />
          </div>

          <div className="flex flex-row gap-2 md:w-[calc(33.3333%-1rem+2px)] md:flex-col md:gap-4">
            <ImagePreview
              src="/missionsList.jpg"
              alt="PayByStep missions list preview"
            />
            <ImagePreview
              src="/missionStep.jpg"
              alt="PayByStep mission preview"
            />
          </div>
        </div>
      ) : (
        <div className="flex flex-col gap-4">
          <Carousel>
            <CarouselContent>
              <CarouselItem>
                <Image
                  className="rounded-lg"
                  src="/homepage.jpg"
                  alt="PayByStep homepage preview"
                  width={1728}
                  height={1117}
                  priority
                />
              </CarouselItem>
              <CarouselItem>
                <Image
                  className="rounded-lg"
                  src="/missionsList.jpg"
                  alt="PayByStep missions list preview"
                  width={1728}
                  height={1117}
                />
              </CarouselItem>
              <CarouselItem>
                <Image
                  className="rounded-lg"
                  src="/missionStep.jpg"
                  alt="PayByStep mission preview"
                  width={1728}
                  height={1117}
                />
              </CarouselItem>
            </CarouselContent>
            <CarouselPoints />
          </Carousel>
        </div>
      )}

      {translations.lastMissionDescription}
    </div>
  );
};

export { LastMission };
