"use client";

import { HTMLAttributes, useEffect, useRef } from "react";
import { Typo } from "@/components/ui/atoms/typo";
import { cn } from "@/lib/utils";
import { useTranslationsStore } from "@/providers/translations-store-provider";
import { useIsVisible } from "@/hooks/use-is-visible";

export interface AboutMeProps extends HTMLAttributes<HTMLDivElement> {}

const AboutMe = ({ className, ...props }: AboutMeProps) => {
  const { translations } = useTranslationsStore((state) => state);
  const ref = useRef(null);
  const isVisible = useIsVisible(ref);
  const isFirstRender = useRef(true);
  const isAppear = useRef(false);

  useEffect(() => {
    isFirstRender.current = false;
    setTimeout(() => {
      isAppear.current = true;
    }, 150);
  }, []);

  return (
    <div
      ref={ref}
      className={cn(
        "flex flex-col gap-4 text-gray-700",
        isVisible ? "animate-appear opacity-100" : "opacity-0",
        !isAppear.current ? "delay-150" : "delay-0",
        className
      )}
      {...props}
    >
      <Typo variant="h2" className="text-gray-950">
        {translations.aboutMeTitle}
      </Typo>

      {translations.aboutMeDescription}
    </div>
  );
};
export { AboutMe };
