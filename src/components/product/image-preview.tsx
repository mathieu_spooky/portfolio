import Image from "next/image";
import {
  ResponsiveDialog,
  ResponsiveDialogTrigger,
  ResponsiveDialogContent,
} from "@/components/ui/molecules/responsive-dialog";
import { Expand } from "lucide-react";
import { GradientBackground } from "../ui/atoms/gradient-background";

const ImagePreview = ({
  src,
  alt,
  priority = false,
}: {
  src: string;
  alt: string;
  priority?: boolean;
}) => {
  return (
    <ResponsiveDialog>
      <ResponsiveDialogTrigger className="ring-offset-background group relative rounded-lg transition-transform duration-300 ease-in-out hover:scale-[1.01] focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-teal-500 focus-visible:ring-offset-2">
        <Image
          className="rounded-lg border border-gray-300 drop-shadow-lg"
          src={src}
          width={1728}
          height={1117}
          alt={alt}
          priority={priority}
        />
        <div className="absolute inset-0 flex items-center justify-center rounded-lg  opacity-0 transition-opacity duration-300 ease-in-out group-hover:opacity-100">
          <GradientBackground className="opacity-20" />
          <Expand size={32} className="text-gray-950" />
        </div>
      </ResponsiveDialogTrigger>
      <ResponsiveDialogContent>
        <div className="p-2">
          <Image
            className="rounded-lg"
            src={src}
            width={1728}
            height={1117}
            alt={alt}
          />
        </div>
      </ResponsiveDialogContent>
    </ResponsiveDialog>
  );
};

export { ImagePreview };
