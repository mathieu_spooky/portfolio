"use client";

import { useState, useEffect, type HTMLAttributes } from "react";
import { cn } from "@/lib/utils";

import { TabsList, TabsTrigger, Tabs } from "@/components/ui/molecules/tabs";
import { Typo } from "@/components/ui/atoms/typo";
import { useMediaQuery } from "@/hooks/use-media-query";
import { useTranslationsStore } from "@/providers/translations-store-provider";
import { LocaleSwitch } from "./locale-switch";
import { Linkedin, Mail } from "lucide-react";
import { Tooltip, TooltipContent, TooltipProvider } from "../ui/atoms/tooltip";
import { TooltipTrigger } from "@radix-ui/react-tooltip";

const elementIds = ["aboutMe", "lastMission", "portfolio", "testimonials"];
export interface HeaderNavigationBarProps
  extends HTMLAttributes<HTMLDivElement> {}

const easeOutFunction = (t: number) => Math.sin((t * Math.PI) / 2);

const HeaderNavigationBar = ({
  className,
  ...props
}: HeaderNavigationBarProps) => {
  const { translations } = useTranslationsStore((state) => state);

  const [scrollPosition, setScrollPosition] = useState(0);
  const [activeTab, setActiveTab] = useState("aboutMe");
  const [inAutomaticScroll, setInAutomaticScroll] = useState(false);
  const [timeoutId, setTimeoutId] = useState<NodeJS.Timeout | null>(null);
  const isDesktop = useMediaQuery("(min-width: 768px)");

  useEffect(() => {
    const handleScroll = () => {
      const position = window.scrollY;
      setScrollPosition(position);
    };

    window.addEventListener("scroll", handleScroll, { passive: true });
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (inAutomaticScroll) return;
    const main = document.querySelector("main");
    if (!main) return;
    const positions = elementIds.map((id) => {
      const element = document.getElementById(id);
      if (!element) {
        return Infinity;
      }
      const top = element.getBoundingClientRect().top;
      if (top < 0) {
        return Infinity;
      }
      return Math.abs(top);
    });
    const minPosition = Math.min(...positions);
    const index =
      minPosition !== Infinity ? positions.indexOf(minPosition) : -1;
    if (activeTab === elementIds[index]) return;
    if (index === -1) {
      setActiveTab(elementIds[elementIds.length - 1]);
      return;
    }
    setActiveTab(elementIds[index]);
  }, [scrollPosition, activeTab, inAutomaticScroll]);

  const handleValueChange = (value: string) => {
    setActiveTab(value);
    const element = document.getElementById(value);
    if (element) {
      setInAutomaticScroll(true);
      element.scrollIntoView({ behavior: "smooth" });
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
      const tid = setTimeout(() => {
        setInAutomaticScroll(false);
      }, 700);
      setTimeoutId(tid);
    }
  };

  const opacity = easeOutFunction(
    Math.min(1, Math.max(0, scrollPosition / 150))
  );
  const backgroundColor = `rgba(255, 255, 255, ${opacity})`;
  const boxShadow = `0 1px 2px 0 rgba(0, 0, 0, ${opacity * 0.1})`;
  const border = `1px solid rgba(0, 0, 0, ${opacity * 0.1})`;

  return (
    <div className={cn("px-2 sm:px-4 drop-shadow-lg", className)} {...props}>
      <div
        className="w-full rounded-b-md"
        style={isDesktop ? { backgroundColor, border, boxShadow } : undefined}
      >
        <div className="container flex items-center justify-center px-4 py-2 sm:justify-start sm:pb-0 md:items-start md:px-8">
          <div className="flex flex-row items-baseline gap-8">
            <Typo variant="h4" className="text-gray-950">
              Mathieu Dervaux
            </Typo>

            <Tabs
              value={activeTab}
              className="hidden md:block"
              onValueChange={handleValueChange}
            >
              <TabsList className="flex w-full justify-between">
                <TabsTrigger value="aboutMe">
                  <Typo variant="p">{translations.aboutMe}</Typo>
                </TabsTrigger>
                <TabsTrigger value="lastMission">
                  <Typo variant="p">{translations.lastMission}</Typo>
                </TabsTrigger>
                <TabsTrigger value="portfolio">
                  <Typo variant="p">{translations.portfolio}</Typo>
                </TabsTrigger>
                <TabsTrigger value="testimonials">
                  <Typo variant="p">{translations.reviews}</Typo>
                </TabsTrigger>
              </TabsList>
            </Tabs>
          </div>

          <div className="ml-auto flex flex-row items-center gap-2">
            <TooltipProvider>
              <Tooltip>
                <TooltipTrigger asChild>
                  <a
                    href="https://www.linkedin.com/in/mathieu-dervaux-224b1b59"
                    className="rounded-lg border border-gray-950 p-1 transition-colors hover:bg-gray-700/20"
                  >
                    <Linkedin className="size-4" />
                  </a>
                </TooltipTrigger>
                <TooltipContent>
                  <Typo variant="p" className="text-gray-white">
                    LinkedIn
                  </Typo>
                </TooltipContent>
              </Tooltip>

              <Tooltip>
                <TooltipTrigger asChild>
                  <a
                    href="mailto:mathieu@spookyrabbits.com"
                    className="mr-1 rounded-lg border border-gray-950 p-1 transition-colors hover:bg-gray-700/20"
                  >
                    <Mail className="size-4" />
                  </a>
                </TooltipTrigger>
                <TooltipContent>
                  <Typo variant="p" className="text-gray-white">
                    Email : mathieu@spookyrabbits.com
                  </Typo>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
            <div className="h-6 w-px bg-gray-950" />
            <LocaleSwitch />
          </div>
        </div>
      </div>
    </div>
  );
};

HeaderNavigationBar.displayName = "Header";

export { HeaderNavigationBar };
