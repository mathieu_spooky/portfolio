"use client";

import { useMemo, useRef } from "react";
import { useIsVisible } from "@/hooks/use-is-visible";

import {
  Testimonial,
  TestimonialContent,
  TestimonialAuthor,
  TestimonialAuthorName,
  TestimonialAuthorJob,
  TestimonialText,
  TestimonialRating,
} from "../ui/molecules/testimonial";

import { useTranslationsStore } from "@/providers/translations-store-provider";
import { cn } from "@/lib/utils";

const Testimonials = () => {
  const ref = useRef(null);
  const isVisible = useIsVisible(ref);
  const { translations } = useTranslationsStore((state) => state);

  const testimonials = useMemo(
    () => [
      {
        id: "1",
        name: "Lou Bettini",
        job: translations.reviewsPayByStepJob,
        message: translations.reviewsPayByStepText,
      },
      {
        id: "2",
        name: "Axelle Seifert",
        job: translations.reviewsClicDataJob,
        message: translations.reviewsClicDataText,
      },
    ],
    [translations]
  );

  return (
    <div
      ref={ref}
      className={cn(
        "flex flex-col gap-4",
        isVisible ? "animate-appear" : "opacity-0"
      )}
    >
      {testimonials.map((testimonial) => (
        <Testimonial key={testimonial.id}>
          <TestimonialContent>
            <TestimonialRating />
            <TestimonialText>
              &ldquo;{testimonial.message}&rdquo;
            </TestimonialText>
            <TestimonialAuthor>
              <TestimonialAuthorName>{testimonial.name}</TestimonialAuthorName>
              <TestimonialAuthorJob>{testimonial.job}</TestimonialAuthorJob>
            </TestimonialAuthor>
          </TestimonialContent>
        </Testimonial>
      ))}
    </div>
  );
};

export { Testimonials };
