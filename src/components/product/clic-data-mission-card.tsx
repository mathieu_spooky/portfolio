"use client";

import { type HTMLAttributes, useRef } from "react";
import Image from "next/image";

import { cn } from "@/lib/utils";
import {
  MissionCard,
  MissionCardContent,
  MissionCardImage,
  MissionHeader,
} from "@/components/product/missions";
import {
  ResponsiveDialog,
  ResponsiveDialogTrigger,
  ResponsiveDialogContent,
} from "@/components/ui/molecules/responsive-dialog";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselPoints,
  CarouselPrevious,
  CarouselNext,
} from "@/components/ui/molecules/carousel";

import { Typo } from "@/components/ui/atoms/typo";
import { useMediaQuery } from "@/hooks/use-media-query";
import { useTranslationsStore } from "@/providers/translations-store-provider";
import { useIsVisible } from "@/hooks/use-is-visible";

export interface ClicDataMissionCardProps
  extends HTMLAttributes<HTMLDivElement> {
  date: string;
}

const ClicDataMissionCard = ({
  className,
  date,
  ...props
}: ClicDataMissionCardProps) => {
  const { translations } = useTranslationsStore((state) => state);
  const isDesktop = useMediaQuery("(min-width: 768px)");
  const ref = useRef(null);
  const isVisible = useIsVisible(ref);

  return (
    <div ref={ref} className={cn(isVisible ? "animate-appear" : "opacity-0")}>
      <ResponsiveDialog>
        <ResponsiveDialogTrigger asChild>
          <MissionCard className={cn("group", className)} {...props}>
            <MissionCardImage>
              {isDesktop ? (
                <Image
                  src="/illustration3.jpg"
                  width={1728}
                  height={1117}
                  alt="clic data illustration"
                />
              ) : (
                <ClicDataCarousel />
              )}
            </MissionCardImage>

            <MissionCardContent>
              <div className="flex flex-col gap-2">
                <MissionHeader
                  name="ClicData"
                  competences={[{ name: "Figma", color: "destructive" }]}
                  date={date}
                />

                <div className="line-clamp-2 text-gray-700">
                  {translations.clicDataDescription}
                </div>
              </div>

              <Typo
                variant="p"
                className="font-bold text-teal-600 underline-offset-4 group-hover:underline"
              >
                En savoir plus
              </Typo>
            </MissionCardContent>
          </MissionCard>
        </ResponsiveDialogTrigger>
        <ResponsiveDialogContent>
          <ClicDataCarousel />
          <div className={`flex flex-col gap-4 ${isDesktop ? "p-8" : "p-4"}`}>
            <MissionHeader
              name="ClicData"
              competences={[{ name: "Figma", color: "destructive" }]}
              date={date}
            />

            <div className="space-y-4 text-gray-700">
              {translations.clicDataDescription}
            </div>
          </div>
        </ResponsiveDialogContent>
      </ResponsiveDialog>
    </div>
  );
};

export { ClicDataMissionCard };

const ClicDataCarousel = () => {
  const isDesktop = useMediaQuery("(min-width: 768px)");
  return (
    <Carousel className={`${isDesktop ? "p-0" : "pt-4"}`}>
      <CarouselContent>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/illustration3.jpg"
            width={1728}
            height={1117}
            alt="clic data illustration"
          />
        </CarouselItem>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/illustration1.jpg"
            width={1728}
            height={1117}
            alt="clic data illustration"
          />
        </CarouselItem>
        <CarouselItem>
          <Image
            className="rounded-lg"
            src="/illustration2.jpg"
            width={1728}
            height={1117}
            alt="clic data illustration"
          />
        </CarouselItem>
      </CarouselContent>
      <CarouselPoints />
      {isDesktop && <CarouselPrevious />}
      {isDesktop && <CarouselNext />}
    </Carousel>
  );
};
