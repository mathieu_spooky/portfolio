"use client";

import { useRef, useEffect } from "react";
import { Typo } from "../ui/atoms/typo";
import { Evaluation } from "../ui/atoms/evaluation";
import Image from "next/image";

import { cn } from "@/lib/utils";
import { useTranslationsStore } from "@/providers/translations-store-provider";
import { useIsVisible } from "@/hooks/use-is-visible";

type TechnologiesItemProps = {
  name: string;
  image: React.ReactNode;
  rate: 0 | 1 | 2 | 3;
};

const imageMap = {
  figma: <Image src="/figma.png" width={26} height={26} alt="figma" />,
  javascript: (
    <Image src="/javascript.png" width={26} height={26} alt="javascript" />
  ),
  react: <Image src="/react.png" width={26} height={26} alt="react" />,
  nextjs: <Image src="/nextjs.png" width={26} height={26} alt="nextjs" />,
  vuejs: <Image src="/vue.png" width={26} height={26} alt="vuejs" />,
  tailwindcss: (
    <Image src="/tailwind.png" width={26} height={26} alt="tailwindcss" />
  ),
};

const TechnologiesItem = ({ name, image, rate }: TechnologiesItemProps) => {
  return (
    <div className="flex items-center gap-2">
      <div className="flex size-8 items-center justify-center">{image}</div>
      <Typo variant="p" className="w-28 text-gray-950">
        <Typo variant="bold">{name}</Typo>
      </Typo>
      <Evaluation rate={rate} />
    </div>
  );
};

type TechnologiesProps = {
  className?: string;
};

const Technologies = ({ className }: TechnologiesProps) => {
  const ref = useRef(null);
  const isVisible = useIsVisible(ref);
  const { translations } = useTranslationsStore((state) => state);
  const isFirstRender = useRef(true);
  useEffect(() => {
    setTimeout(() => {
      isFirstRender.current = false;
    }, 150);
  }, []);
  return (
    <div
      className={cn(
        "flex flex-col gap-2",
        isVisible ? "animate-appear" : "opacity-0",
        isFirstRender.current ? "delay-150" : "delay-0",
        className
      )}
      ref={ref}
    >
      <Typo variant="h2" className="mb-2 text-gray-950">
        {translations.technologiesTitle}
      </Typo>
      <TechnologiesItem name="Figma" image={imageMap.figma} rate={3} />
      <TechnologiesItem
        name="JavaScript"
        image={imageMap.javascript}
        rate={3}
      />
      <TechnologiesItem name="React" image={imageMap.react} rate={3} />
      <TechnologiesItem name="Next.js" image={imageMap.nextjs} rate={2} />
      <TechnologiesItem name="Vue.js" image={imageMap.vuejs} rate={1} />
      <TechnologiesItem
        name="Tailwindcss"
        image={imageMap.tailwindcss}
        rate={3}
      />
    </div>
  );
};

export { Technologies };
