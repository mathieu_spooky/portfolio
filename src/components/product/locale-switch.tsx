"use client";

import { type HTMLAttributes } from "react";
import { useLocale } from "next-intl";
import { cn } from "@/lib/utils";

import { locales, usePathname, useRouter } from "@/i18n";
import { Typo } from "@/components/ui/atoms/typo";

export interface LocaleSwitchProps extends HTMLAttributes<HTMLButtonElement> {}

const LocaleSwitch = ({ className, ...props }: LocaleSwitchProps) => {
  const locale = useLocale();
  const pathname = usePathname();
  const router = useRouter();

  const handleLocaleChange = () => {
    const localeIndex = locales.indexOf(locale);
    const nextLocale = locales[(localeIndex + 1) % locales.length];
    router.push(pathname, { locale: nextLocale });
  };

  return (
    <button
      className={cn(
        "rounded-lg px-1 py-1 transition-all duration-200 hover:bg-gray-700/20",
        className
      )}
      onClick={handleLocaleChange}
      {...props}
    >
      {locales.map((l, index) => (
        <span key={index}>
          <Typo
            className={l === locale ? "text-gray-900" : "text-gray-700"}
            key={l}
            variant={l === locale ? "bold" : "span"}
          >
            {l}
          </Typo>
          {index === locales.length - 1 ? "" : " / "}
        </span>
      ))}
    </button>
  );
};

export { LocaleSwitch };
