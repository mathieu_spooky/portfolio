import { type ReactNode, createElement } from "react";

import { cva, type VariantProps } from "class-variance-authority";
import { cn } from "@/lib/utils";

type TypoElement = "h1" | "h2" | "h3" | "h4" | "h5" | "p" | "span";

const TypoVariants = cva("font-sans", {
  variants: {
    variant: {
      h1: "font-title text-3xl font-bold md:text-4xl",
      h2: "font-title text-2xl font-bold md:text-3xl",
      h3: "font-title text-lg font-bold md:text-xl",
      h4: "font-title text-base font-bold md:text-lg",
      h5: "font-title text-sm font-bold md:text-base",
      p: "font-body text-sm font-normal md:text-base",
      span: "font-body text-sm font-normal md:text-base",
      bold: "font-body font-bold",
    },
    monospace: {
      true: "font-monospace tracking-tighter",
    },
  },
  defaultVariants: {
    variant: "p",
    monospace: false,
  },
});

export interface TypoProps extends VariantProps<typeof TypoVariants> {
  children: ReactNode;
  className?: string;
  id?: string;
}

// type is variant key
type TypoVariants = Exclude<
  VariantProps<typeof TypoVariants>["variant"],
  null | undefined
>;

const elementMap = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  p: "p",
  span: "span",
  bold: "span",
} satisfies {
  [K in TypoVariants]: TypoElement;
};

const Typo = ({ children, variant, monospace, className, id }: TypoProps) => {
  if (variant === null || variant === undefined) {
    throw new Error("variant prop is required");
  }

  return createElement(
    elementMap[variant],
    { className: cn(TypoVariants({ variant, monospace }), className), id },
    children
  );
};

export { Typo };
