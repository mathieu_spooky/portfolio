import { type HTMLAttributes } from "react";
import { cn } from "@/lib/utils";

export interface EvaluationProps extends HTMLAttributes<HTMLDivElement> {
  rate: 0 | 1 | 2 | 3;
}

const TealLine = () => {
  return <div className="h-[2px] flex-1 rounded-full bg-teal-500" />;
};

const GrayLine = () => {
  return <div className="h-[2px] flex-1 rounded-full bg-gray-300" />;
};

const Evaluation = ({ className, rate, ...props }: EvaluationProps) => {
  return (
    <div
      className={cn("flex flex-1 flex-row items-center gap-2", className)}
      {...props}
    >
      {rate > 0 ? <TealLine /> : <GrayLine />}
      {rate > 1 ? <TealLine /> : <GrayLine />}
      {rate > 2 ? <TealLine /> : <GrayLine />}
    </div>
  );
};

export { Evaluation };
