import { type HTMLAttributes } from "react";
import { cn } from "@/lib/utils";

export interface GradientBackgroundProps
  extends HTMLAttributes<HTMLDivElement> {
  position?: "fixed" | "absolute" | "initial";
}

const GradientBackground = ({
  className,
  position = "absolute",
  ...props
}: GradientBackgroundProps) => {
  return (
    <>
      <div
        className={cn(
          `${position} top-0 size-full bg-gradient-to-bl from-cyan-300 to-teal-300`,
          className
        )}
        {...props}
      />
      <div
        className={cn(
          `${position} top-0 size-full bg-gradient-to-tl from-indigo-300/60 to-cyan-300/10 to-40%`,
          className
        )}
        {...props}
      />
    </>
  );
};

export { GradientBackground };
