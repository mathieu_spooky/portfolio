import { type HTMLAttributes } from "react";
import { cn } from "@/lib/utils";
import { Typo } from "@/components/ui/atoms/typo";

interface TestimonialProps extends HTMLAttributes<HTMLDivElement> {}

const Testimonial = ({ children, className, ...props }: TestimonialProps) => {
  return (
    <div
      className={cn(
        "flex-1 relative bg-gray-50 rounded-lg border border-gray-300 shadow-lg",
        className
      )}
      {...props}
    >
      {children}
    </div>
  );
};

interface TestimonialRatingProps extends HTMLAttributes<HTMLDivElement> {}

const TestimonialRating = ({ ...props }: TestimonialRatingProps) => {
  return (
    <div {...props}>
      <svg
        width="132"
        height="24"
        viewBox="0 0 132 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M10 2L13.09 8.26L20 9.27L15 14.14L16.18 21.02L10 17.77L3.82 21.02L5 14.14L0 9.27L6.91 8.26L10 2Z"
          fill="url(#paint0_linear_46_5)"
        />
        <path
          d="M38 2L41.09 8.26L48 9.27L43 14.14L44.18 21.02L38 17.77L31.82 21.02L33 14.14L28 9.27L34.91 8.26L38 2Z"
          fill="url(#paint1_linear_46_5)"
        />
        <path
          d="M66 2L69.09 8.26L76 9.27L71 14.14L72.18 21.02L66 17.77L59.82 21.02L61 14.14L56 9.27L62.91 8.26L66 2Z"
          fill="url(#paint2_linear_46_5)"
        />
        <path
          d="M94 2L97.09 8.26L104 9.27L99 14.14L100.18 21.02L94 17.77L87.82 21.02L89 14.14L84 9.27L90.91 8.26L94 2Z"
          fill="url(#paint3_linear_46_5)"
        />
        <path
          d="M122 2L125.09 8.26L132 9.27L127 14.14L128.18 21.02L122 17.77L115.82 21.02L117 14.14L112 9.27L118.91 8.26L122 2Z"
          fill="url(#paint4_linear_46_5)"
        />
        <defs>
          <linearGradient
            id="paint0_linear_46_5"
            x1="8.677e-08"
            y1="12"
            x2="131.5"
            y2="12"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#0D9488" />
            <stop offset="1" stopColor="#0891B2" />
          </linearGradient>
          <linearGradient
            id="paint1_linear_46_5"
            x1="-0.499998"
            y1="12"
            x2="132"
            y2="12"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#0D9488" />
            <stop offset="1" stopColor="#0891B2" />
          </linearGradient>
          <linearGradient
            id="paint2_linear_46_5"
            x1="6.52988e-08"
            y1="12"
            x2="132"
            y2="12"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#0D9488" />
            <stop offset="1" stopColor="#0891B2" />
          </linearGradient>
          <linearGradient
            id="paint3_linear_46_5"
            x1="4.03448e-06"
            y1="12"
            x2="132"
            y2="12"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#0D9488" />
            <stop offset="1" stopColor="#0891B2" />
          </linearGradient>
          <linearGradient
            id="paint4_linear_46_5"
            x1="0.499998"
            y1="12"
            x2="132"
            y2="12"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#0D9488" />
            <stop offset="1" stopColor="#0891B2" />
          </linearGradient>
        </defs>
      </svg>
    </div>
  );
};

const TestimonialContent = ({
  children,
  className,
  ...props
}: TestimonialProps) => {
  return (
    <div
      className={cn(
        "h-full flex flex-col gap-4 p-6 z-30 justify-between",
        className
      )}
      {...props}
    >
      {children}
    </div>
  );
};

const TestimonialAuthor = ({
  children,
  className,
  ...props
}: TestimonialProps) => {
  return (
    <div
      className={cn(
        "flex flex-col gap-0 self-start text-transparent bg-clip-text bg-gradient-to-tr from-teal-600 to-cyan-600 mt-auto",
        className
      )}
      {...props}
    >
      {children}
    </div>
  );
};

const TestimonialAuthorName = ({
  children,
  className,
  ...props
}: TestimonialProps) => {
  return (
    <Typo variant="h3" className={cn("", className)} {...props}>
      {children}
    </Typo>
  );
};

const TestimonialAuthorJob = ({
  children,
  className,
  ...props
}: TestimonialProps) => {
  return (
    <Typo variant="p" className={cn("", className)} {...props}>
      {children}
    </Typo>
  );
};

const TestimonialText = ({
  children,
  className,
  ...props
}: TestimonialProps) => {
  return (
    <Typo variant="p" className={cn("text-gray-700", className)} {...props}>
      {children}
    </Typo>
  );
};

export {
  Testimonial,
  TestimonialRating,
  TestimonialContent,
  TestimonialAuthor,
  TestimonialAuthorName,
  TestimonialAuthorJob,
  TestimonialText,
};
