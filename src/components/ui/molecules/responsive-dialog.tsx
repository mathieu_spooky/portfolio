"use client";

import { type ElementRef, type ComponentPropsWithoutRef } from "react";
import { useMediaQuery } from "@/hooks/use-media-query";
import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogDescription,
} from "@/components/ui/molecules/dialog";
import {
  Drawer,
  DrawerTrigger,
  DrawerContent,
  DrawerHeader,
  DrawerTitle,
  DrawerDescription,
} from "@/components/ui/molecules/drawer";
import { forwardRef } from "react";

type ResponsiveDialogProps = React.ComponentProps<typeof Drawer>;

const ResponsiveDialog = ({ children, ...props }: ResponsiveDialogProps) => {
  const isDesktop = useMediaQuery("(min-width: 768px)");

  if (!isDesktop) {
    return <Drawer {...props}>{children}</Drawer>;
  }

  return <Dialog {...props}>{children}</Dialog>;
};

const ResponsiveDialogTrigger = forwardRef<
  ElementRef<typeof DrawerTrigger>,
  ComponentPropsWithoutRef<typeof DrawerTrigger>
>(({ children, ...props }, ref) => {
  const isDesktop = useMediaQuery("(min-width: 768px)");

  if (!isDesktop) {
    return (
      <DrawerTrigger ref={ref} {...props}>
        {children}
      </DrawerTrigger>
    );
  }

  return (
    <DialogTrigger ref={ref} {...props}>
      {children}
    </DialogTrigger>
  );
});

ResponsiveDialogTrigger.displayName = "ResponsiveDialogTrigger";

type ResponsiveDialogContentProps = React.ComponentProps<typeof DrawerContent>;

const ResponsiveDialogContent = ({
  className,
  children,
  ...props
}: ResponsiveDialogContentProps) => {
  const isDesktop = useMediaQuery("(min-width: 768px)");

  if (!isDesktop) {
    return (
      <DrawerContent className={className} {...props}>
        {children}
      </DrawerContent>
    );
  }

  return (
    <DialogContent className={className} {...props}>
      {children}
    </DialogContent>
  );
};

type ResponsiveDialogHeaderProps = React.ComponentProps<typeof DrawerHeader>;

const ResponsiveDialogHeader = ({
  children,
  ...props
}: ResponsiveDialogHeaderProps) => {
  const isDesktop = useMediaQuery("(min-width: 768px)");

  if (!isDesktop) {
    return <DrawerHeader {...props}>{children}</DrawerHeader>;
  }

  return <DialogHeader {...props}>{children}</DialogHeader>;
};

type ResponsiveDialogTitleProps = React.ComponentProps<typeof DrawerTitle>;

const ResponsiveDialogTitle = ({
  children,
  ...props
}: ResponsiveDialogTitleProps) => {
  const isDesktop = useMediaQuery("(min-width: 768px)");

  if (!isDesktop) {
    return <DrawerTitle {...props}>{children}</DrawerTitle>;
  }

  return <DialogTitle {...props}>{children}</DialogTitle>;
};

type ResponsiveDialogDescriptionProps = React.ComponentProps<
  typeof DrawerDescription
>;

const ResponsiveDialogDescription = ({
  children,
  ...props
}: ResponsiveDialogDescriptionProps) => {
  const isDesktop = useMediaQuery("(min-width: 768px)");

  if (!isDesktop) {
    return <DrawerDescription {...props}>{children}</DrawerDescription>;
  }

  return <DialogDescription {...props}>{children}</DialogDescription>;
};

export {
  ResponsiveDialog,
  ResponsiveDialogTrigger,
  ResponsiveDialogContent,
  ResponsiveDialogHeader,
  ResponsiveDialogTitle,
  ResponsiveDialogDescription,
};
