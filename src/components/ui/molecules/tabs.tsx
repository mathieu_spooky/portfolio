"use client";

import {
  forwardRef,
  ComponentPropsWithoutRef,
  ElementRef,
  Dispatch,
  SetStateAction,
  useState,
  useEffect,
  useRef,
  createContext,
  useContext,
} from "react";
import { Root, List, Trigger, Content } from "@radix-ui/react-tabs";

import { cn } from "@/lib/utils";

type TabsContext<
  TTabsTriggerElement extends ElementRef<typeof Trigger> = ElementRef<
    typeof Trigger
  >,
> = {
  hoverTabElement: TTabsTriggerElement | null;
  setHoverTabElement: Dispatch<SetStateAction<TTabsTriggerElement | null>>;
  selectedElement: TTabsTriggerElement | null;
  setSelectedElement: Dispatch<SetStateAction<TTabsTriggerElement | null>>;
  selectedValue: string;
  setSelectedValue: Dispatch<SetStateAction<string>>;
};

const TabsContext = createContext<TabsContext>({} as TabsContext);

const Tabs = forwardRef<
  ElementRef<typeof Root>,
  ComponentPropsWithoutRef<typeof Root>
>(({ className, value, defaultValue, onValueChange, ...props }, ref) => {
  const [hoverTabElement, setHoverTabElement] = useState<ElementRef<
    typeof Trigger
  > | null>(null);

  const [selectedElement, setSelectedElement] = useState<ElementRef<
    typeof Trigger
  > | null>(null);

  const [selectedValue, setSelectedValue] = useState(
    value || defaultValue || ""
  );

  const handleValueChange = (value: string) => {
    setSelectedValue(value);
    if (onValueChange) {
      onValueChange(value);
    }
  };

  useEffect(() => {
    if (value) {
      setSelectedValue(value);
    }
  }, [value]);

  return (
    <TabsContext.Provider
      value={{
        hoverTabElement,
        setHoverTabElement,
        selectedElement,
        setSelectedElement,
        selectedValue,
        setSelectedValue,
      }}
    >
      <Root
        ref={ref}
        className={className}
        value={value}
        defaultValue={defaultValue}
        onValueChange={handleValueChange}
        {...props}
      />
    </TabsContext.Provider>
  );
});
Tabs.displayName = Root.displayName;

const TabsList = forwardRef<
  ElementRef<typeof List>,
  ComponentPropsWithoutRef<typeof List>
>(({ className, children, ...props }, ref) => {
  const tabsContext = useContext(TabsContext);

  const hoverBackgroundRef = useRef<HTMLDivElement | null>(null);
  const selectedBarRef = useRef<HTMLDivElement | null>(null);
  const prevHoverTabRef = useRef<TabsContext["hoverTabElement"] | null>(null);

  useEffect(() => {
    if (tabsContext.hoverTabElement) {
      const width = tabsContext.hoverTabElement.offsetWidth;
      const height = tabsContext.hoverTabElement.offsetHeight;
      const left = tabsContext.hoverTabElement.offsetLeft;

      hoverBackgroundRef.current!.style.width = `${width}px`;
      hoverBackgroundRef.current!.style.height = `${height}px`;
      hoverBackgroundRef.current!.style.transform = `translateX(${left}px)`;

      if (prevHoverTabRef.current) {
        hoverBackgroundRef.current!.style.transitionProperty =
          "transform, width";
        hoverBackgroundRef.current!.style.transitionDuration = "0.2s";
        hoverBackgroundRef.current!.style.transitionTimingFunction = "ease-out";
      }
    }
  }, [tabsContext.hoverTabElement]);

  useEffect(() => {
    if (prevHoverTabRef.current !== tabsContext.hoverTabElement) {
      prevHoverTabRef.current = tabsContext.hoverTabElement;
    }
  }, [tabsContext.hoverTabElement]);

  useEffect(() => {
    if (tabsContext.selectedElement) {
      const width = tabsContext.selectedElement.offsetWidth;
      const left = tabsContext.selectedElement.offsetLeft;
      selectedBarRef.current!.style.width = `${width}px`;
      selectedBarRef.current!.style.transform = `translateX(${left}px)`;
    }
  }, [tabsContext.selectedElement]);

  return (
    <List
      ref={ref}
      className={cn("relative inline-flex pt-1 pb-2 text-gray-700", className)}
      {...props}
    >
      {children}

      {tabsContext.hoverTabElement && (
        <div
          ref={hoverBackgroundRef}
          className={cn(
            "absolute inset-0 mt-1 bg-gray-700/20 rounded-lg pointer-events-none  opacity-0",
            tabsContext.hoverTabElement && "opacity-100"
          )}
        />
      )}

      <div
        ref={selectedBarRef}
        className="absolute -bottom-px left-0 h-[2px] bg-gray-900 transition-all"
      />
    </List>
  );
});

TabsList.displayName = "TabsList";

const TabsTrigger = forwardRef<
  ElementRef<typeof Trigger>,
  ComponentPropsWithoutRef<typeof Trigger>
>(({ className, value, ...props }, ref) => {
  const tabsContext = useContext(TabsContext);
  const triggerRef = useRef<ElementRef<typeof Trigger> | null>(null);
  const handleMouseEnter = () => {
    tabsContext.setHoverTabElement(triggerRef.current);
  };

  const handleMouseLeave = () => {
    tabsContext.setHoverTabElement(null);
  };

  useEffect(() => {
    if (value === tabsContext.selectedValue) {
      tabsContext.setSelectedElement(triggerRef.current);
    }
  }, [tabsContext, value]);

  return (
    <Trigger
      ref={(node) => {
        triggerRef.current = node;
        if (typeof ref === "function") {
          ref(node);
        } else if (ref) {
          ref.current = node;
        }
      }}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      value={value}
      className={cn(
        "font-title inline-flex items-center justify-center whitespace-nowrap rounded-md px-2 py-1 text-xs font-medium ring-offset-background transition-all focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-teal-500 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 data-[state=active]:text-gray-950 hover:text-gray-950",
        className
      )}
      {...props}
    />
  );
});

TabsTrigger.displayName = Trigger.displayName;

const TabsContent = forwardRef<
  ElementRef<typeof Content>,
  ComponentPropsWithoutRef<typeof Content>
>(({ className, ...props }, ref) => (
  <Content
    ref={ref}
    className={cn(
      "mt-2 ring-offset-background focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2",
      className
    )}
    {...props}
  />
));
TabsContent.displayName = Content.displayName;

export { Tabs, TabsList, TabsTrigger, TabsContent };
