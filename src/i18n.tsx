import { notFound } from "next/navigation";
import { getRequestConfig } from "next-intl/server";
import { createSharedPathnamesNavigation } from "next-intl/navigation";
import { Typo } from "@/components/ui/atoms/typo";

export const locales = ["en", "fr"];
export const localePrefix = "always";
export const { Link, redirect, usePathname, useRouter } =
  createSharedPathnamesNavigation({ locales, localePrefix });

export type Locale = (typeof locales)[number];

export default getRequestConfig(async ({ locale }: { locale: string }) => {
  if (!locales.includes(locale)) notFound();

  return {
    defaultTranslationValues: {
      p: (children) => <Typo variant="p">{children}</Typo>,
      bold: (children) => <Typo variant="bold">{children}</Typo>,
    },
    messages: (await import(`../messages/${locale}.json`)).default,
  };
});
